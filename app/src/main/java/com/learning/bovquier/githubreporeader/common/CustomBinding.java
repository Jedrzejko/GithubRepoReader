package com.learning.bovquier.githubreporeader.common;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * @author bovquier
 *         on 16.10.2017.
 */

public class CustomBinding {

    @BindingAdapter("imageUrl")
    public static void setupImage(ImageView imgView, String url) {
        Glide.with(imgView.getContext()).load(url).into(imgView);
    }
}
