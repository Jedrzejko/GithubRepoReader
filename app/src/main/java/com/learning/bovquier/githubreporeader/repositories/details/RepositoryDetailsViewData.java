package com.learning.bovquier.githubreporeader.repositories.details;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.learning.bovquier.githubreporeader.BR;
import com.learning.bovquier.githubreporeader.datamodels.Repository;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public class RepositoryDetailsViewData extends BaseObservable {
    private String name;
    private String description;
    private String fullName;
    private String homepage;
    private String language;
    private String extendedName;
    private String imgUrl;
    private int homePageVisibility = VISIBLE;
    private int fieldsVisibility = GONE;
    private int progressVisibility = VISIBLE;

    @Bindable
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    public void setData(Repository data) {
        name = data.getName();
        description = data.getDescription();
        fullName = data.getFullName();
        homepage = data.getHomepage();
        language = data.getLanguage();
        extendedName = name + "/" + fullName;
        notifyChange();
    }

    @Bindable
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        notifyPropertyChanged(BR.description);
    }

    @Bindable
    @SuppressWarnings("unused")
    public String getFullName() {
        return fullName == null ? "" : ": " + fullName;
    }

    @SuppressWarnings("unused")
    public void setFullName(String fullName) {
        this.fullName = fullName;
        notifyPropertyChanged(BR.fullName);
    }

    @Bindable
    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
        setHomePageVisibility(homepage);
        notifyPropertyChanged(BR.homepage);
    }

    @Bindable
    @SuppressWarnings("unused")
    public int getHomePageVisibility() {
        return homePageVisibility;
    }

    private void setHomePageVisibility(String homepage) {
        this.homePageVisibility = homepage.isEmpty() ? GONE : VISIBLE;
        notifyPropertyChanged(BR.homePageVisibility);
    }

    @Bindable
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
        notifyPropertyChanged(BR.language);
    }

    void setupLoadingVisibilities(boolean visible) {
        progressVisibility = visible ? VISIBLE : GONE;
        fieldsVisibility = visible ? GONE : VISIBLE;
        notifyPropertyChanged(BR.progressVisibility);
        notifyPropertyChanged(BR.fieldsVisibility);
    }

    @Bindable
    public int getFieldsVisibility() {
        return fieldsVisibility;
    }

    @Bindable
    public int getProgressVisibility() {
        return progressVisibility;
    }

    @Bindable
    public String getExtendedName() {
        return extendedName;
    }

    @SuppressWarnings("unused")
    public void setExtendedName(String extendedName) {
        this.extendedName = extendedName;
    }

    @Bindable
    public String getImgUrl() {
        return imgUrl;
    }

    void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
        notifyPropertyChanged(BR.imgUrl);
    }
}
