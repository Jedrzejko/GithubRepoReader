package com.learning.bovquier.githubreporeader.app;

import android.content.Context;

import dagger.Module;
import dagger.Provides;

/**
 * @author bovquier
 *         on 16.09.2017.
 */

@Module
class ApplicationModule {

    private final RepositoriesViewerApplication mApplication;

    ApplicationModule(RepositoriesViewerApplication application) {
        mApplication = application;
    }

    @Provides
    RepositoriesViewerApplication provideApplication() {
        return mApplication;
    }

    @Provides
    Context provideContext() {
        return mApplication;
    }
}
