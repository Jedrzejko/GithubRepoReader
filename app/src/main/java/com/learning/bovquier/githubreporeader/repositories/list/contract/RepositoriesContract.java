package com.learning.bovquier.githubreporeader.repositories.list.contract;

import com.learning.bovquier.githubreporeader.base.BaseContract;
import com.learning.bovquier.githubreporeader.datamodels.Repository;

import java.util.List;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public interface RepositoriesContract {
    interface View extends BaseContract.View {
        void showData(List<Repository> data);

        void openItem(Repository adapterPosition);
    }

    interface Model extends BaseContract.Model<List<Repository>> {
        void loadRepositoriesList();
    }

    interface Presenter<V extends BaseContract.View, M extends BaseContract.Model>
            extends BaseContract.Presenter<V, M> {
        void loadRepositories();
    }

    interface Service extends BaseContract.Service<List<Repository>> {
    }

    interface Listener {
        void onRepoClicked(Repository repository);
    }
}
