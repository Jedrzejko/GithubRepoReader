package com.learning.bovquier.githubreporeader.common;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public class NotOkResponseException extends Throwable {
    public NotOkResponseException(String msg) {
        super(msg);
    }
}
