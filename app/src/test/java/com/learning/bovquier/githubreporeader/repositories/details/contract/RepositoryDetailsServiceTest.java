package com.learning.bovquier.githubreporeader.repositories.details.contract;

import com.learning.bovquier.githubreporeader.datamodels.Repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import retrofit2.Call;
import retrofit2.Callback;

import static org.mockito.Mockito.verify;

/**
 * @author bovquier
 *         on 11.10.2017.
 */
public class RepositoryDetailsServiceTest {
    @Mock
    private Call<Repository> call;
    @Mock
    private Callback<Repository> callback;
    @Mock
    private RepositoryDetailsPresenter.RepositoryDetailsObserver mObserver;
    @InjectMocks
    private RepositoryDetailsService repositoryDetailsService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        repositoryDetailsService.callback = callback;
        repositoryDetailsService.call = call;
    }

    @Test
    public void testStopService() throws Exception {
        repositoryDetailsService.stopService();
        verify(call).cancel();
    }

    @Test
    public void testLoad() throws Exception {
        repositoryDetailsService.load();
        verify(call).enqueue(callback);
    }

    @Test
    public void testAddRequestObserver() throws Exception {
        repositoryDetailsService.addRequestObserver(mObserver);
        Assert.assertEquals(mObserver, repositoryDetailsService.getObserver());
    }
}