package com.learning.bovquier.githubreporeader.di.details;

import com.learning.bovquier.githubreporeader.api.GitHubApi;
import com.learning.bovquier.githubreporeader.common.ConnectionStateProvider;
import com.learning.bovquier.githubreporeader.repositories.details.RepositoryDetailsViewData;
import com.learning.bovquier.githubreporeader.repositories.details.contract.RepositoryDetailsModel;
import com.learning.bovquier.githubreporeader.repositories.details.contract.RepositoryDetailsPresenter;
import com.learning.bovquier.githubreporeader.repositories.details.contract.RepositoryDetailsService;

import dagger.Module;
import dagger.Provides;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

@Module
public class RepositoryDetailsModule {

    @Provides
    RepositoryDetailsPresenter providePresenter(RepositoryDetailsModel model,
                                                ConnectionStateProvider connectionStateProvider) {
        return new RepositoryDetailsPresenter(model, connectionStateProvider);
    }

    @Provides
    RepositoryDetailsModel provideModel(RepositoryDetailsService service) {
        return new RepositoryDetailsModel(service);
    }

    @Provides
    RepositoryDetailsService provideService(GitHubApi api) {
        return new RepositoryDetailsService(api);
    }

    @Provides
    RepositoryDetailsViewData provideViewData() {
        return new RepositoryDetailsViewData();
    }
}
