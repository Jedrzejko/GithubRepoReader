package com.learning.bovquier.githubreporeader.repositories.details.contract;

import com.learning.bovquier.githubreporeader.api.GitHubApi;
import com.learning.bovquier.githubreporeader.base.BaseService;
import com.learning.bovquier.githubreporeader.datamodels.Repository;

import retrofit2.Call;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public class RepositoryDetailsService extends BaseService<Repository>
        implements RepositoryDetailsContract.Service {
    Call<Repository> call;
    private GitHubApi mApi;

    public RepositoryDetailsService(GitHubApi api) {
        mApi = api;
    }

    @Override
    public void stopService() {
        call.cancel();
    }

    @Override
    public void load() {
        if (call != null) {
            call.enqueue(callback);
        }
    }

    @Override
    public RepositoryDetailsContract.Service buildRepository(Repository repository) {
        call = mApi.getRepositoryDetails(repository.getOwner().getLogin(), repository.getName());
        return this;
    }

    @Override
    protected void resetCall(Call<Repository> call) {
        this.call = call.clone();
    }
}
