package com.learning.bovquier.githubreporeader.base;

import com.learning.bovquier.githubreporeader.common.NotOkResponseException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * @author bovquier
 *         on 20.09.2017.
 */

public abstract class BaseService<DM> implements BaseContract.Service<DM> {

    public Callback<DM> callback;

    private BaseRequestObserver<DM> mObserver;

    {
        callback = createCallback();
    }

    @Override
    public void addRequestObserver(BaseRequestObserver<DM> observer) {
        mObserver = observer;
    }

    @Override
    public BaseRequestObserver<DM> getObserver() {
        return mObserver;
    }

    public Callback<DM> createCallback() {
        return new Callback<DM>() {
            @Override
            public void onResponse(Call<DM> call, Response<DM> response) {
                manageResponse(call, response);
            }

            @Override
            public void onFailure(Call<DM> call, Throwable t) {
                resetCall(call);
                showError(t);
            }
        };
    }

    private void manageResponse(Call<DM> call, Response<DM> response) {
        if (response.code() == 200) {
            getObserver().onResponseBodyReceived(response.body());
        } else {
            resetCall(call);
            getObserver().onRequestFailure(new NotOkResponseException(
                    "Error while loading data: code:" + response.code()));
        }
    }

    protected abstract void resetCall(Call<DM> call);

    private void showError(Throwable t) {
        getObserver().onRequestFailure(t);
    }
}
