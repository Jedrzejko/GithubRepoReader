package com.learning.bovquier.githubreporeader.repositories.details.contract;

import com.learning.bovquier.githubreporeader.datamodels.Repository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author bovquier
 *         on 11.10.2017.
 */
public class RepositoryDetailsModelTest {
    @Mock
    private RepositoryDetailsService mService;
    @Mock
    private Repository mRepository;
    @InjectMocks
    private RepositoryDetailsModel repositoryDetailsModel;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        repositoryDetailsModel.setService(mService);
    }

    @Test
    public void testLoadRepositoryDetails() throws Exception {
        when(mService.buildRepository(mRepository)).thenReturn(mService);
        repositoryDetailsModel.loadRepositoryDetails(mRepository);
        verify(mService).buildRepository(mRepository);
        verify(mService).load();
    }

    @Test
    public void testCancelCall() throws Exception {
        repositoryDetailsModel.cancelCall();
        verify(mService).stopService();
    }
}