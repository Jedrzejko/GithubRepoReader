package com.learning.bovquier.githubreporeader.repositories.list.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.learning.bovquier.githubreporeader.R;
import com.learning.bovquier.githubreporeader.databinding.ItemRepositoryBinding;
import com.learning.bovquier.githubreporeader.datamodels.Repository;
import com.learning.bovquier.githubreporeader.repositories.list.contract.RepositoriesContract;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoryViewHolder> {
    private List<Repository> items = new ArrayList<>();
    private RepositoriesContract.Listener clickListener;
    private List<Repository> originalList;

    @Override
    public RepositoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemRepositoryBinding dataBinding = DataBindingUtil
                .inflate(layoutInflater, R.layout.item_repository, parent, false);
        return new RepositoryViewHolder(dataBinding, clickListener);
    }

    @Override
    public void onBindViewHolder(RepositoryViewHolder holder, int position) {
        holder.bindValues(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items == null ? 0 : items.size();
    }

    public void setItems(List<Repository> data) {
        items.clear();
        items.addAll(data);
        originalList = new ArrayList<>(data);
        notifyDataSetChanged();
    }

    public void setClickListener(RepositoriesContract.Listener clickListener) {
        this.clickListener = clickListener;
    }

    public void filterItems(String searchPhrase) {
        filter(originalList, searchPhrase);
        notifyDataSetChanged();
    }

    private void filter(final List<Repository> originalList, final String searchPhrase) {
        Observable.fromIterable(originalList).filter(new Predicate<Repository>() {
            @Override
            public boolean test(Repository repository) throws Exception {
                return repository.getFullName().contains(searchPhrase) ||
                       repository.getName().contains(searchPhrase);
            }
        }).toList().observeOn(Schedulers.computation()).subscribe(new Consumer<List<Repository>>() {
            @Override
            public void accept(List<Repository> repositories) throws Exception {
                items.clear();
                items.addAll(repositories);
            }
        });
    }
}
