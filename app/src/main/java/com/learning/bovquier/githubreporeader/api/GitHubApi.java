package com.learning.bovquier.githubreporeader.api;

import com.learning.bovquier.githubreporeader.datamodels.Repository;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public interface GitHubApi {
    @Headers({"Accept:application/vnd.github.v3+json"})
    @GET("users/{user}/repos")
    Call<List<Repository>> getRepositoriesList(@Path("user") String user,
                                               @Query("sort") String sort,
                                               @Query("direction") String direction,
                                               @Query("type") String type);

    @Headers({"Accept:application/vnd.github.v3+json"})
    @GET("repos/{owner}/{repo}")
    Call<Repository> getRepositoryDetails(@Path("owner") String owner, @Path("repo") String repo);
}
