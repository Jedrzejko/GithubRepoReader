package com.learning.bovquier.githubreporeader.repositories.list.contract;

import com.learning.bovquier.githubreporeader.api.GitHubApi;
import com.learning.bovquier.githubreporeader.base.BaseService;
import com.learning.bovquier.githubreporeader.datamodels.Repository;

import java.util.List;

import retrofit2.Call;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public class RepositoriesListService extends BaseService<List<Repository>>
        implements RepositoriesContract.Service {
    private static final String SORT = "updated";
    private static final String DIR = "desc";
    private static final String TYPE = "owner";
    private Call<List<Repository>> call;

    public RepositoriesListService(GitHubApi api) {
        call = api.getRepositoriesList("square", SORT, DIR, TYPE);
    }

    @Override
    public void stopService() {
        call.cancel();
    }

    @Override
    public void load() {
        call.enqueue(callback);
    }

    @Override
    protected void resetCall(Call<List<Repository>> call) {
        this.call = call.clone();
    }
}
