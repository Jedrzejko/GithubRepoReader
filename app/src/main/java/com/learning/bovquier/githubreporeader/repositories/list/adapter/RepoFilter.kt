package com.learning.bovquier.githubreporeader.repositories.list.adapter

import com.learning.bovquier.githubreporeader.datamodels.Repository

/**
 * @author bovquier
 * on 16.10.2017.
 */

class RepoFilter {
    fun filterItems(originalItems: MutableList<out Repository>, searchPhrase: String): List<Repository> {
        return originalItems.filter { it.matchSearchPhrase(searchPhrase) }
    }
}

fun Repository.matchSearchPhrase(searchPhrase: String): Boolean {
    return this.fullName.toLowerCase().contains(searchPhrase) || this.name.toLowerCase().contains(searchPhrase)
}
