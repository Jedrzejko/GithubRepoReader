package com.learning.bovquier.githubreporeader.app;

import android.app.Application;

import com.learning.bovquier.githubreporeader.di.details.DaggerRepositoryDetailsComponent;
import com.learning.bovquier.githubreporeader.di.details.RepositoryDetailsComponent;
import com.learning.bovquier.githubreporeader.di.details.RepositoryDetailsModule;
import com.learning.bovquier.githubreporeader.di.repositories.DaggerRepositoriesFragmentComponent;
import com.learning.bovquier.githubreporeader.di.repositories.RepositoriesFragmentComponent;
import com.learning.bovquier.githubreporeader.di.repositories.RepositoriesFragmentModule;

/**
 * @author bovquier
 *         on 16.09.2017.
 */

public class RepositoriesViewerApplication extends Application {

    private RepositoriesFragmentComponent repositoriesFragmentComponent;
    private RepositoryDetailsComponent repositoryDetailsComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        ApplicationComponent component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();
        repositoriesFragmentComponent = DaggerRepositoriesFragmentComponent.builder()
                .applicationComponent(component)
                .repositoriesFragmentModule(new RepositoriesFragmentModule()).build();
        repositoryDetailsComponent = DaggerRepositoryDetailsComponent.builder()
                .applicationComponent(component)
                .repositoryDetailsModule(new RepositoryDetailsModule()).build();
    }

    public RepositoriesFragmentComponent getRepositoriesComponent() {
        return repositoriesFragmentComponent;
    }

    public RepositoryDetailsComponent getRepositoryDetailsComponent() {
        return repositoryDetailsComponent;
    }
}
