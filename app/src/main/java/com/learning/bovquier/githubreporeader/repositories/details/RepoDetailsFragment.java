package com.learning.bovquier.githubreporeader.repositories.details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.learning.bovquier.githubreporeader.R;
import com.learning.bovquier.githubreporeader.base.BaseContract;
import com.learning.bovquier.githubreporeader.base.BaseFragment;
import com.learning.bovquier.githubreporeader.databinding.FragmentRepoDetailsBinding;
import com.learning.bovquier.githubreporeader.datamodels.Repository;
import com.learning.bovquier.githubreporeader.repositories.details.contract.RepositoryDetailsContract;
import com.learning.bovquier.githubreporeader.repositories.details.contract.RepositoryDetailsPresenter;

import javax.inject.Inject;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public class RepoDetailsFragment extends BaseFragment<FragmentRepoDetailsBinding>
        implements RepositoryDetailsContract.View {
    private static final String KEY_REPO = "KEY_REPO";

    @Inject
    RepositoryDetailsPresenter mPresenter;

    @Inject
    RepositoryDetailsViewData mViewData;

    public static RepoDetailsFragment getInstance(Repository item) {
        RepoDetailsFragment fragment = new RepoDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_REPO, item);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        getApplication().getRepositoryDetailsComponent().inject(this);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_repo_details;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadRepoDetails((Repository) getArguments().getParcelable(KEY_REPO));
    }

    @Override
    protected BaseContract.Presenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void attachViewToPresenter() {
        mPresenter.setView(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getBinding().setViewData(mViewData);
    }

    @Override
    public void setData(Repository data) {
        mViewData.setData(data);
        mViewData.setImgUrl(data.getOwner().getAvatarUrl());
        getParentView().setTitle(data.getName());
    }

    @Override
    public void showProgress(boolean visible) {
        mViewData.setupLoadingVisibilities(visible);
    }
}
