package com.learning.bovquier.githubreporeader.repositories.list.contract;

import android.view.View;

import com.learning.bovquier.githubreporeader.common.ConnectionStateProvider;
import com.learning.bovquier.githubreporeader.common.NotOkResponseException;
import com.learning.bovquier.githubreporeader.datamodels.Repository;
import com.learning.bovquier.githubreporeader.repositories.list.adapter.RepositoryViewHolder;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.nio.channels.NotYetConnectedException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author bovquier
 *         on 11.10.2017.
 */
public class RepositoriesListPresenterTest {
    @Mock
    private RepositoriesContract.View mView;
    @Mock
    private RepositoriesListModel mModel;
    @Mock
    private View mAndroidView;
    @Mock
    private ConnectionStateProvider mStateProvider;
    @Mock
    private RepositoryViewHolder mViewHolder;
    @Mock
    private Repository mRepository;
    @InjectMocks
    private RepositoriesListPresenter repositoriesListPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        repositoriesListPresenter.setModel(mModel);
        repositoriesListPresenter.setView(mView);
    }

    @Test
    public void testReloadWhenOnline() throws Exception {
        when(mStateProvider.isOnline()).thenReturn(true);
        repositoriesListPresenter.reload();
        verify(mStateProvider).isOnline();
        verify(mModel).loadRepositoriesList();
    }

    @Test
    public void testReloadWhenOffline() throws Exception {
        when(mStateProvider.isOnline()).thenReturn(false);
        repositoriesListPresenter.reload();
        verify(mStateProvider).isOnline();
        verify(mView).showErrorMsg(any(NotYetConnectedException.class));
    }

    @Test
    public void testOnClick() throws Exception {
        when(mAndroidView.getTag()).thenReturn(mViewHolder);
        repositoriesListPresenter.onRepoClicked(mRepository);
        verify(mView).openItem(mRepository);
    }

    @Test
    public void testStopService() throws Exception {
        repositoriesListPresenter.stopService();
        verify(mModel).cancelCall();
    }

    @Test
    public void testObserverSucceededWithExceededList() {
        List<Repository> responseList = getReposnseList(25);
        repositoriesListPresenter.manageRecievedResponse(responseList);
        verify(mView).showProgress(false);
        verify(mView).showData(responseList.subList(0, 20));
    }

    private List<Repository> getReposnseList(int amount) {
        List<Repository> result = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            result.add(new Repository());
        }
        return result;
    }

    @Test
    public void testObserverSucceededWithMatchedListSize() {
        List<Repository> responseList = getReposnseList(10);
        repositoriesListPresenter.manageRecievedResponse(responseList);
        verify(mView).showProgress(false);
        verify(mView).showData(responseList);
    }

    @Test
    public void testObserverFailure() {
        NotOkResponseException notOkResponseException = any(NotOkResponseException.class);
        repositoriesListPresenter.manageFailureReposnse(notOkResponseException);
        verify(mView).showProgress(false);
        verify(mView).showErrorMsg(notOkResponseException);
    }
}