package com.learning.bovquier.githubreporeader.repositories.details.contract;

import com.learning.bovquier.githubreporeader.common.ConnectionStateProvider;
import com.learning.bovquier.githubreporeader.common.NotOkResponseException;
import com.learning.bovquier.githubreporeader.datamodels.Repository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.nio.channels.NotYetConnectedException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @author bovquier
 *         on 11.10.2017.
 */
public class RepositoryDetailsPresenterTest {
    @Mock
    private Repository mRepository;
    @Mock
    private RepositoryDetailsContract.View mView;
    @Mock
    private RepositoryDetailsModel mModel;
    @Mock
    private ConnectionStateProvider mStateProvider;
    @InjectMocks
    private RepositoryDetailsPresenter repositoryDetailsPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        repositoryDetailsPresenter.setModel(mModel);
        repositoryDetailsPresenter.setView(mView);
    }

    @Test
    public void testReloadWhenOnline() throws Exception {
        when(mStateProvider.isOnline()).thenReturn(true);
        repositoryDetailsPresenter.mRepository = mRepository;
        repositoryDetailsPresenter.reload();
        verify(mStateProvider).isOnline();
        verify(mModel).loadRepositoryDetails(mRepository);
    }

    @Test
    public void testReloadWhenOffline() throws Exception {
        when(mStateProvider.isOnline()).thenReturn(false);
        repositoryDetailsPresenter.reload();
        verify(mStateProvider).isOnline();
        verify(mView).showErrorMsg(any(NotYetConnectedException.class));
    }

    @Test
    public void testLoadRepoDetailsWhenOnline() throws Exception {
        when(mStateProvider.isOnline()).thenReturn(true);
        repositoryDetailsPresenter.loadRepoDetails(mRepository);
        verify(mStateProvider).isOnline();
        verify(mModel).loadRepositoryDetails(mRepository);
    }

    @Test
    public void testStopService() throws Exception {
        repositoryDetailsPresenter.stopService();
        verify(mModel).cancelCall();
    }

    @Test
    public void testObserverSucceededWithMatchedListSize() {
        repositoryDetailsPresenter.manageSucceededReposnse(mRepository);
        verify(mView).showProgress(false);
        verify(mView).setData(mRepository);
    }

    @Test
    public void testObserverFailure() {
        NotOkResponseException notOkResponseException = any(NotOkResponseException.class);
        repositoryDetailsPresenter.manageFailureResponse(notOkResponseException);
        verify(mView).showProgress(false);
        verify(mView).showErrorMsg(notOkResponseException);
    }
}