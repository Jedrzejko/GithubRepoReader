package com.learning.bovquier.githubreporeader.repositories.list;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.learning.bovquier.githubreporeader.BR;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public class RepositoriesData extends BaseObservable {
    private int progressVisibility = VISIBLE;
    private int listVisibility = GONE;
    private int emptyViewVisibility = GONE;

    @Bindable
    public int getProgressVisibility() {
        return progressVisibility;
    }

    public void setProgressVisibile(boolean progressVisible) {
        this.progressVisibility = progressVisible ? VISIBLE : GONE;
        notifyPropertyChanged(BR.progressVisibility);
    }

    @Bindable
    public int getListVisibility() {
        return listVisibility;
    }

    public void setListVisibile(boolean listVisibility) {
        this.listVisibility = listVisibility ? VISIBLE : GONE;
        notifyPropertyChanged(BR.listVisibility);
    }

    public void setEmptyViewVisible(boolean emptyViewVisible) {
        emptyViewVisibility = emptyViewVisible ? VISIBLE : GONE;
        notifyPropertyChanged(BR.emptyViewVisibility);
    }

    @Bindable
    public int getEmptyViewVisibility() {
        return emptyViewVisibility;
    }
}
