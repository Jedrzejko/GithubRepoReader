package com.learning.bovquier.githubreporeader.base;

/**
 * @author bovquier
 *         on 16.09.2017.
 */

public abstract class BaseModel<DM> implements BaseContract.Model<DM> {
    private BaseContract.Service<DM> mService;

    @Override
    public void addRequestObserver(BaseRequestObserver<DM> observer) {
        mService.addRequestObserver(observer);
    }

    public BaseContract.Service<DM> getService() {
        return mService;
    }

    @Override
    public void cancelCall() {
        getService().stopService();
    }

    public void setService(BaseContract.Service<DM> service) {
        mService = service;
    }
}
