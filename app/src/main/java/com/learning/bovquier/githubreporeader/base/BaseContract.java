package com.learning.bovquier.githubreporeader.base;

import retrofit2.Callback;

/**
 * @author bovquier
 *         on 15.09.2017.
 */

public interface BaseContract {
    interface Presenter<V extends View, M extends Model> {
        V getView();

        void setView(V view);

        M getModel();

        void setModel(M model);

        void stopService();

        void reload();
    }

    interface View {
        void showProgress(boolean visible);

        void hideKeyboard();

        void showErrorMsg(Throwable t);
    }

    interface Model<DM> {
        void addRequestObserver(BaseRequestObserver<DM> observer);

        BaseContract.Service<DM> getService();

        void cancelCall();
    }

    interface Service<DM> {
        void stopService();

        void load();

        void addRequestObserver(BaseRequestObserver<DM> observer);

        BaseRequestObserver<DM> getObserver();

        Callback<DM> createCallback();
    }
}
