package com.learning.bovquier.githubreporeader.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * @author bovquier
 *         on 16.10.2017.
 */

public class ConnectionStateProvideImpl implements ConnectionStateProvider {
    private Context context;

    public ConnectionStateProvideImpl(Context context) {
        this.context = context;
    }

    @Override
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = null;
        if (cm != null) {
            netInfo = cm.getActiveNetworkInfo();
        }
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
