package com.learning.bovquier.githubreporeader.base;

import com.learning.bovquier.githubreporeader.datamodels.Repository;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public interface BaseActivityView extends BaseContract.View {
    void openItem(Repository item);

    void setTitle(String name);
}
