package com.learning.bovquier.githubreporeader.base;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * @author bovquier
 *         on 15.09.2017.
 */

public abstract class BaseActivity<B extends ViewDataBinding> extends AppCompatActivity
        implements BaseContract.View {

    private B binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getLayoutId());
    }

    @LayoutRes
    protected abstract int getLayoutId();

    @Override
    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        if (inputManager != null) {
            View currentFocus = getCurrentFocus();
            if (currentFocus != null) {
                inputManager.hideSoftInputFromWindow(currentFocus.getWindowToken(),
                                                     InputMethodManager.RESULT_UNCHANGED_SHOWN
                );
            }
        }
    }

    public B getBinding() {
        return binding;
    }

    public Object getPresenter() {
        return null;
    }
}
