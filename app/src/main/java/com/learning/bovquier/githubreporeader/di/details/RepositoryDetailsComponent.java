package com.learning.bovquier.githubreporeader.di.details;

import com.learning.bovquier.githubreporeader.app.ApplicationComponent;
import com.learning.bovquier.githubreporeader.di.UserScope;
import com.learning.bovquier.githubreporeader.di.repositories.RepositoriesFragmentModule;
import com.learning.bovquier.githubreporeader.repositories.details.RepoDetailsFragment;

import dagger.Component;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

@UserScope
@Component(dependencies = ApplicationComponent.class,
           modules = {RepositoriesFragmentModule.class, RepositoryDetailsModule.class})
public interface RepositoryDetailsComponent {
    void inject(RepoDetailsFragment fragment);
}
