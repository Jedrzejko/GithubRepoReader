package com.learning.bovquier.githubreporeader.repositories.list.adapter;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.learning.bovquier.githubreporeader.datamodels.Repository;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public class RepositoryItemViewData extends BaseObservable {
    private String name;
    private String htmlUrl;
    private Repository mRepository;

    void setItemData(Repository repository) {
        name = repository.getName();
        htmlUrl = repository.getHtmlUrl();
        mRepository = repository;
        notifyChange();
    }

    @Bindable
    public String getName() {
        return name;
    }

    @Bindable
    public String getHtmlUrl() {
        return htmlUrl;
    }

    public Repository getRepository() {
        return mRepository;
    }
}
