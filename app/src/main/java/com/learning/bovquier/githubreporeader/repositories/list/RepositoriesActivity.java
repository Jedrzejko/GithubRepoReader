package com.learning.bovquier.githubreporeader.repositories.list;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.learning.bovquier.githubreporeader.R;
import com.learning.bovquier.githubreporeader.base.BaseActivity;
import com.learning.bovquier.githubreporeader.base.BaseActivityView;
import com.learning.bovquier.githubreporeader.databinding.ActivityRepositoriesBinding;
import com.learning.bovquier.githubreporeader.datamodels.Repository;
import com.learning.bovquier.githubreporeader.repositories.details.RepoDetailsFragment;

public class RepositoriesActivity extends BaseActivity<ActivityRepositoriesBinding>
        implements BaseActivityView {

    private static final String TAG_DETAILS = "TAG_DETAILS";
    private static final String TAG_LIST = "TAG_LIST";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(getBinding().toolbar);
        replaceFragment(new RepositoriesFragment(), TAG_LIST);
    }

    private void replaceFragment(Fragment fragment, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fl_container, fragment, tag).addToBackStack(tag).commit();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_repositories;
    }

    @Override
    public void openItem(Repository item) {
        replaceFragment(RepoDetailsFragment.getInstance(item), TAG_DETAILS);
    }

    @Override
    public void setTitle(String name) {
        getBinding().toolbar.setTitle(name);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
            getBinding().toolbar.setTitle(R.string.title_repositories);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void showProgress(boolean visible) {
        //no-op
    }

    @Override
    public void showErrorMsg(Throwable t) {
        //no-op
    }
}
