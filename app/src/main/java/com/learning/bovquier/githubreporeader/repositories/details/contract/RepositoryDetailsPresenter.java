package com.learning.bovquier.githubreporeader.repositories.details.contract;

import com.learning.bovquier.githubreporeader.base.BasePresenter;
import com.learning.bovquier.githubreporeader.base.BaseRequestObserver;
import com.learning.bovquier.githubreporeader.common.ConnectionStateProvider;
import com.learning.bovquier.githubreporeader.datamodels.Repository;

import java.nio.channels.NotYetConnectedException;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public class RepositoryDetailsPresenter
        extends BasePresenter<Repository, RepositoryDetailsContract.View, RepositoryDetailsModel>
        implements RepositoryDetailsContract.Presenter {
    private final ConnectionStateProvider mConnectionStateProvider;
    Repository mRepository;

    public RepositoryDetailsPresenter(RepositoryDetailsModel model,
                                      ConnectionStateProvider connectionStateProvider) {
        super(model);
        mConnectionStateProvider = connectionStateProvider;
    }

    @Override
    protected BaseRequestObserver<Repository> createObserver() {
        return new RepositoryDetailsObserver();
    }

    @Override
    public void reload() {
        load();
    }

    private void load() {
        if (mConnectionStateProvider.isOnline()) {
            getModel().loadRepositoryDetails(mRepository);
        } else {
            getView().showErrorMsg(new NotYetConnectedException());
        }
    }

    @Override
    public void loadRepoDetails(Repository repository) {
        mRepository = repository;
        load();
    }

    void manageFailureResponse(Throwable t) {
        getView().showProgress(false);
        getView().showErrorMsg(t);
    }

    void manageSucceededReposnse(Repository response) {
        getView().showProgress(false);
        getView().setData(response);
    }

    class RepositoryDetailsObserver extends BaseRequestObserver<Repository> {
        @Override
        public void onResponseBodyReceived(Repository response) {
            manageSucceededReposnse(response);
        }

        @Override
        public void onRequestFailure(Throwable t) {
            manageFailureResponse(t);
        }
    }
}
