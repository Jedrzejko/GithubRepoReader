package com.learning.bovquier.githubreporeader.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.learning.bovquier.githubreporeader.R;
import com.learning.bovquier.githubreporeader.app.RepositoriesViewerApplication;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public abstract class BaseFragment<B extends ViewDataBinding> extends Fragment
        implements BaseContract.View {

    private B binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        return binding.getRoot();
    }

    @LayoutRes
    protected abstract int getLayoutId();

    @Override
    public void onResume() {
        super.onResume();
        attachViewToPresenter();
    }

    @Override
    public void onPause() {
        super.onPause();
        BaseContract.Presenter presenter = getPresenter();
        if (presenter != null) presenter.stopService();
    }

    protected abstract BaseContract.Presenter getPresenter();

    protected abstract void attachViewToPresenter();

    protected RepositoriesViewerApplication getApplication() {
        return (RepositoriesViewerApplication) getActivity().getApplication();
    }

    @Override
    public void hideKeyboard() {
        getParentView().hideKeyboard();
    }

    public BaseActivityView getParentView() {
        return (BaseActivityView) getActivity();
    }

    @Override
    public void showErrorMsg(Throwable t) {
        Snackbar.make(getBinding().getRoot(), t.getMessage(), Snackbar.LENGTH_LONG)
                .setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getPresenter().reload();
                    }
                }).show();
    }

    public B getBinding() {
        return binding;
    }
}
