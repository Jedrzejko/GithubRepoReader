package com.learning.bovquier.githubreporeader.common;

/**
 * @author bovquier
 *         on 16.10.2017.
 */

public interface ConnectionStateProvider {
    boolean isOnline();
}
