package com.learning.bovquier.githubreporeader.app;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;

/**
 * @author bovquier
 *         on 16.09.2017.
 */

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    void inject(RepositoriesViewerApplication application);

    Context getContext();
}
