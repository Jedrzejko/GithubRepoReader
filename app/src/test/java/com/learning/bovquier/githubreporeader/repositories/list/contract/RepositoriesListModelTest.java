package com.learning.bovquier.githubreporeader.repositories.list.contract;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

/**
 * @author bovquier
 *         on 11.10.2017.
 */
public class RepositoriesListModelTest {
    @Mock
    RepositoriesListService mService;
    @Mock
    RepositoriesListPresenter.RepositoriesListObserver mObserver;
    @InjectMocks
    RepositoriesListModel repositoriesListModel;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        repositoriesListModel.setService(mService);
    }

    @Test
    public void testLoadRepositoriesList() throws Exception {
        repositoriesListModel.loadRepositoriesList();
        verify(mService).load();
    }

    @Test
    public void testAddRequestObserver() throws Exception {
        repositoriesListModel.addRequestObserver(mObserver);
        verify(mService).addRequestObserver(mObserver);
    }

    @Test
    public void testCancelCall() throws Exception {
        repositoriesListModel.cancelCall();
        verify(mService).stopService();
    }
}