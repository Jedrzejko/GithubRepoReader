package com.learning.bovquier.githubreporeader.repositories.details.contract;

import com.learning.bovquier.githubreporeader.base.BaseContract;
import com.learning.bovquier.githubreporeader.datamodels.Repository;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public interface RepositoryDetailsContract {
    interface View extends BaseContract.View {
        void setData(Repository data);
    }

    interface Model extends BaseContract.Model<Repository> {
        void loadRepositoryDetails(Repository repository);
    }

    interface Service extends BaseContract.Service<Repository> {
        Service buildRepository(Repository repository);
    }

    interface Presenter {
        void loadRepoDetails(Repository repository);
    }
}
