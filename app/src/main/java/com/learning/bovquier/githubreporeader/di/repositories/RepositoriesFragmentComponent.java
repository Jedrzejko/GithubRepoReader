package com.learning.bovquier.githubreporeader.di.repositories;

import com.learning.bovquier.githubreporeader.app.ApplicationComponent;
import com.learning.bovquier.githubreporeader.di.UserScope;
import com.learning.bovquier.githubreporeader.repositories.list.RepositoriesFragment;

import dagger.Component;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

@UserScope
@Component(dependencies = ApplicationComponent.class, modules = {RepositoriesFragmentModule.class})
public interface RepositoriesFragmentComponent {
    void inject(RepositoriesFragment fragment);
}
