package com.learning.bovquier.githubreporeader.di;

import javax.inject.Scope;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

@Scope
public @interface UserScope {
}
