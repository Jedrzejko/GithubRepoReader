package com.learning.bovquier.githubreporeader.base;

/**
 * @author bovquier
 *         on 16.09.2017.
 */

public abstract class BaseRequestObserver<DM> {
    public abstract void onResponseBodyReceived(DM response);

    public abstract void onRequestFailure(Throwable t);
}
