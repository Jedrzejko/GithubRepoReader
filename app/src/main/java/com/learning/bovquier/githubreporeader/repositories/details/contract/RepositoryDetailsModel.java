package com.learning.bovquier.githubreporeader.repositories.details.contract;

import com.learning.bovquier.githubreporeader.base.BaseModel;
import com.learning.bovquier.githubreporeader.datamodels.Repository;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public class RepositoryDetailsModel extends BaseModel<Repository>
        implements RepositoryDetailsContract.Model {
    public RepositoryDetailsModel(RepositoryDetailsService service) {
        setService(service);
    }

    @Override
    public void loadRepositoryDetails(Repository repository) {
        ((RepositoryDetailsService) getService()).buildRepository(repository).load();
    }
}
