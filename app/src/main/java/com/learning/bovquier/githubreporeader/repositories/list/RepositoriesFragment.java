package com.learning.bovquier.githubreporeader.repositories.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.learning.bovquier.githubreporeader.R;
import com.learning.bovquier.githubreporeader.base.BaseContract;
import com.learning.bovquier.githubreporeader.base.BaseFragment;
import com.learning.bovquier.githubreporeader.common.SimpleDividerDecoration;
import com.learning.bovquier.githubreporeader.databinding.FragmentRepositoriesBinding;
import com.learning.bovquier.githubreporeader.datamodels.Repository;
import com.learning.bovquier.githubreporeader.repositories.list.adapter.RepositoriesAdapter;
import com.learning.bovquier.githubreporeader.repositories.list.contract.RepositoriesContract;
import com.learning.bovquier.githubreporeader.repositories.list.contract.RepositoriesListPresenter;

import java.util.List;

import javax.inject.Inject;

/**
 * A placeholder fragment containing a simple view.
 */
public class RepositoriesFragment extends BaseFragment<FragmentRepositoriesBinding>
        implements RepositoriesContract.View {

    @Inject
    RepositoriesListPresenter mPresenter;
    @Inject
    RepositoriesData repositoriesData;
    @Inject
    RepositoriesAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupBinder();
        setupRecycler();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search, menu);
        SearchView svSearch = (SearchView) menu.findItem(R.id.search).getActionView();
        svSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                mAdapter.filterItems(s);
                getParentView().hideKeyboard();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                mAdapter.filterItems(s);
                return true;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void setupBinder() {
        getBinding().setViewData(repositoriesData);
        getBinding().setPresenter(mPresenter);
    }

    private void setupRecycler() {
        mAdapter.setClickListener(mPresenter);
        getBinding().rvRepositories.setLayoutManager(new LinearLayoutManager(getContext()));
        getBinding().rvRepositories.setHasFixedSize(true);
        getBinding().rvRepositories.addItemDecoration(new SimpleDividerDecoration(
                ContextCompat.getDrawable(getContext(), R.drawable.divider)));
        getBinding().rvRepositories.setAdapter(mAdapter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        getApplication().getRepositoriesComponent().inject(this);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_repositories;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadRepositories();
    }

    @Override
    protected BaseContract.Presenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void attachViewToPresenter() {
        mPresenter.setView(this);
    }

    @Override
    public void showProgress(boolean progressVisible) {
        repositoriesData.setProgressVisibile(progressVisible);
        repositoriesData.setListVisibile(!progressVisible);
    }

    @Override
    public void showData(List<Repository> data) {
        if (data.isEmpty()) {
            repositoriesData.setListVisibile(false);
            repositoriesData.setEmptyViewVisible(true);
        } else {
            mAdapter.setItems(data);
        }
    }

    @Override
    public void openItem(Repository adapterPosition) {
        getParentView().openItem(adapterPosition);
    }
}
