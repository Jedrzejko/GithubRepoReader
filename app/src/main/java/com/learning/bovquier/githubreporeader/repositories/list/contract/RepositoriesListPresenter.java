package com.learning.bovquier.githubreporeader.repositories.list.contract;

import android.support.annotation.VisibleForTesting;

import com.learning.bovquier.githubreporeader.base.BasePresenter;
import com.learning.bovquier.githubreporeader.base.BaseRequestObserver;
import com.learning.bovquier.githubreporeader.common.ConnectionStateProvider;
import com.learning.bovquier.githubreporeader.datamodels.Repository;

import java.nio.channels.NotYetConnectedException;
import java.util.List;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public class RepositoriesListPresenter extends
        BasePresenter<List<Repository>, RepositoriesContract.View, RepositoriesContract.Model>
        implements
        RepositoriesContract.Presenter<RepositoriesContract.View, RepositoriesContract.Model>,
        RepositoriesContract.Listener {
    private ConnectionStateProvider mStateProvider;

    public RepositoriesListPresenter(RepositoriesContract.Model model,
                                     ConnectionStateProvider stateProvider) {
        super(model);
        mStateProvider = stateProvider;
    }

    @Override
    protected BaseRequestObserver<List<Repository>> createObserver() {
        return new RepositoriesListObserver();
    }

    @Override
    public void reload() {
        load();
    }

    private void load() {
        if (mStateProvider.isOnline()) {
            getModel().loadRepositoriesList();
        } else {
            getView().showErrorMsg(new NotYetConnectedException());
        }
    }

    public void loadRepositories() {
        load();
    }

    @VisibleForTesting
    void manageFailureReposnse(Throwable t) {
        getView().showProgress(false);
        getView().showErrorMsg(t);
    }

    @VisibleForTesting
    void manageRecievedResponse(List<Repository> data) {
        getView().showProgress(false);
        boolean exceeded = data.size() > 20;
        getView().showData(exceeded ? data.subList(0, 20) : data);
        //GitHub Api v.3 provides 30 results per page - unable to set limit for request. Api
        // v4. provides limits, but needs a lot of additional code to maintain GraphQL request
    }

    @Override
    public void onRepoClicked(Repository repository) {
        getView().openItem(repository);
    }

    class RepositoriesListObserver extends BaseRequestObserver<List<Repository>> {
        @Override
        public void onResponseBodyReceived(List<Repository> data) {
            manageRecievedResponse(data);
        }

        @Override
        public void onRequestFailure(Throwable t) {
            manageFailureReposnse(t);
        }
    }
}
