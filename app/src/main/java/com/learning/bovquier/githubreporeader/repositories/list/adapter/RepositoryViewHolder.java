package com.learning.bovquier.githubreporeader.repositories.list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.learning.bovquier.githubreporeader.databinding.ItemRepositoryBinding;
import com.learning.bovquier.githubreporeader.datamodels.Repository;
import com.learning.bovquier.githubreporeader.repositories.list.contract.RepositoriesContract;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public class RepositoryViewHolder extends RecyclerView.ViewHolder {

    private final RepositoryItemViewData viewData;

    RepositoryViewHolder(ItemRepositoryBinding dataBinding,
                         final RepositoriesContract.Listener clickListener) {
        super(dataBinding.getRoot());
        viewData = new RepositoryItemViewData();
        dataBinding.setViewData(viewData);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onRepoClicked(viewData.getRepository());
            }
        });
        itemView.setTag(this);
    }

    void bindValues(Repository item) {
        viewData.setItemData(item);
    }
}
