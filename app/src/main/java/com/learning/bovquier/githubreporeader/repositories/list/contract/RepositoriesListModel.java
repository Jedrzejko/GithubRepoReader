package com.learning.bovquier.githubreporeader.repositories.list.contract;

import com.learning.bovquier.githubreporeader.base.BaseModel;
import com.learning.bovquier.githubreporeader.datamodels.Repository;

import java.util.List;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

public class RepositoriesListModel extends BaseModel<List<Repository>>
        implements RepositoriesContract.Model {

    public RepositoriesListModel(RepositoriesContract.Service service) {
        setService(service);
    }

    @Override
    public void loadRepositoriesList() {
        getService().load();
    }
}
