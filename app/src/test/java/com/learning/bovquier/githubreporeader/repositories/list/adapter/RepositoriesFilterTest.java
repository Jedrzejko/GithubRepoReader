package com.learning.bovquier.githubreporeader.repositories.list.adapter;

import com.learning.bovquier.githubreporeader.datamodels.Repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author bovquier
 *         on 11.10.2017.
 */
public class RepositoriesFilterTest {
    @Mock
    RepositoriesAdapter mAdapter;
    RepoFilter repositoriesFilter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        repositoriesFilter = new RepoFilter();
    }

    @Test
    public void testGetFilteredData() throws Exception {
        List<Repository> filteredItems = repositoriesFilter.filterItems(getItems(), "5");
        Assert.assertEquals(filteredItems, Collections.singletonList(getRepository(5)));
    }

    private List<Repository> getItems() {
        List<Repository> result = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            result.add(getRepository(i));
        }
        return result;
    }

    private Repository getRepository(int i) {
        Repository repository = new Repository();
        repository.setId(i);
        if (i % 2 == 0) {
            repository.setName("qaz" + String.valueOf(i));
            repository.setFullName("123");
        } else {
            repository.setName("678");
            repository.setFullName(String.valueOf(i) + "tgb");
        }
        return repository;
    }
}