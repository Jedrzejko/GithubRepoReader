package com.learning.bovquier.githubreporeader.di.repositories;

import android.content.Context;

import com.learning.bovquier.githubreporeader.api.GitHubApi;
import com.learning.bovquier.githubreporeader.common.ConnectionStateProvideImpl;
import com.learning.bovquier.githubreporeader.common.ConnectionStateProvider;
import com.learning.bovquier.githubreporeader.repositories.list.RepositoriesData;
import com.learning.bovquier.githubreporeader.repositories.list.adapter.RepositoriesAdapter;
import com.learning.bovquier.githubreporeader.repositories.list.contract.RepositoriesContract;
import com.learning.bovquier.githubreporeader.repositories.list.contract.RepositoriesContract.Model;
import com.learning.bovquier.githubreporeader.repositories.list.contract.RepositoriesListModel;
import com.learning.bovquier.githubreporeader.repositories.list.contract.RepositoriesListPresenter;
import com.learning.bovquier.githubreporeader.repositories.list.contract.RepositoriesListService;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author bovquier
 *         on 10.10.2017.
 */

@Module
public class RepositoriesFragmentModule {

    private static final String BASE_URL = "https://api.github.com/";

    @Provides
    RepositoriesListPresenter providePresenter(Model model, ConnectionStateProvider stateProvider) {
        return new RepositoriesListPresenter(model, stateProvider);
    }

    @Provides
    RepositoriesData provideRepositoryData() {
        return new RepositoriesData();
    }

    @Provides
    RepositoriesAdapter provideAdapter() {
        return new RepositoriesAdapter();
    }

    @Provides
    RepositoriesContract.Model provideRepositoriesModel(RepositoriesContract.Service service) {
        return new RepositoriesListModel(service);
    }

    @Provides
    RepositoriesContract.Service provideService(GitHubApi api) {
        return new RepositoriesListService(api);
    }

    @Provides
    GitHubApi provideApi() {
        return provideRetrofit(provideClient()).create(GitHubApi.class);
    }

    private Retrofit provideRetrofit(OkHttpClient client) {
        return new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(client).build();
    }

    private OkHttpClient provideClient() {
        return new OkHttpClient.Builder().followRedirects(true).build();
    }

    @Provides
    ConnectionStateProvider providerStateProvider(Context context) {
        return new ConnectionStateProvideImpl(context);
    }
}
