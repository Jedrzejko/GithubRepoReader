package com.learning.bovquier.githubreporeader.base;

import com.learning.bovquier.githubreporeader.base.BaseContract.Model;
import com.learning.bovquier.githubreporeader.base.BaseContract.View;

/**
 * @author bovquier
 *         on 16.09.2017.
 */

public abstract class BasePresenter<DM, V extends View, M extends Model<DM>>
        implements BaseContract.Presenter<V, M> {
    private V mView;
    private M mModel;

    public BasePresenter(M model) {
        setModel(model);
    }

    @Override
    public V getView() {
        return mView;
    }

    @Override
    public void setView(V view) {
        mView = view;
    }

    @Override
    public M getModel() {
        return mModel;
    }

    @Override
    public void setModel(M model) {
        mModel = model;
        mModel.addRequestObserver(createObserver());
    }

    protected abstract BaseRequestObserver<DM> createObserver();

    @Override
    public void stopService() {
        getModel().cancelCall();
    }
}
